package error

import "errors"

var (
	containerErrMap = make(map[byte]error)
)

const (
	UncreatedContainerErr = 0x00
	CreatedContainerErr   = 0x01
)

func init() {
	containerErrMap[UncreatedContainerErr] = errors.New("[docker] 容器尚未创建，不得执行操作")
	containerErrMap[CreatedContainerErr] = errors.New("[docker] 已创建的容器，不得再次创建")
}

func ContainerErr(byte2 byte) error {
	return containerErrMap[byte2]
}
