package main

import (
	"glybrain-go"
	"glybrain-go/utils"
)

func main() {

	glybrain_go.NewGlyBrainService("/")
	cont := utils.NewContainer("test2")
	_ = cont.Stop()
	_ = cont.Remove()

	err := cont.Create(19133)
	defer cont.Stop()
	defer cont.Remove()
	if err != nil {
		utils.GetLogger().Error(err)
	}
	utils.ListContainer()

	err = cont.Start()
	if err != nil {
		utils.GetLogger().Error(err)
	}
	utils.ListContainer()

	glybrain_go.Srv.Start()
}
