package utils

import (
	log "github.com/sirupsen/logrus"
	"os"
)

var logger *log.Logger

func init() {
	logger = log.New()
	// Log as JSON instead of the default ASCII formatter.
	logger.SetFormatter(&log.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: "15:04:05",
		DisableColors:   false,
	})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logger.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	logger.SetLevel(log.DebugLevel)

}

func GetLogger() *log.Logger {
	return logger
}
