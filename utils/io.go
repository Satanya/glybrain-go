package utils

import (
	"io"
	"os"
	"path/filepath"
	"strings"
)

func CopyDir(src string, dest string) error {
	src_original := src
	err := filepath.Walk(src, func(src string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}
		if f.IsDir() {
			CopyDir(f.Name(), dest+"/"+f.Name())
		} else {
			destNew := strings.Replace(src, src_original, dest, -1)
			CopyFile(src, destNew)
		}
		//println(path)
		return nil
	})
	return err
}

//egodic directories
func GetFilelist(path string) {
	err := filepath.Walk(path, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}
		if f.IsDir() {
			return nil
		}
		println(path)
		return nil
	})
	if err != nil {
		panic(err)
	}
}
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

//copy file
func CopyFile(src, dst string) (w int64, err error) {
	srcFile, err := os.Open(src)
	if err != nil {
		panic(err)
		return
	}
	defer srcFile.Close()
	dst_slices := strings.Split(dst, "\\")
	dst_slices_len := len(dst_slices)
	dest_dir := ""
	for i := 0; i < dst_slices_len-1; i++ {
		dest_dir = dest_dir + dst_slices[i] + "\\"
	}
	//dest_dir := getParentDirectory(dst)
	b, err := PathExists(dest_dir)
	if b == false {
		err := os.Mkdir(dest_dir, os.ModePerm) //在当前目录下生成md目录
		if err != nil {
			panic(err)
		}
	}
	dstFile, err := os.Create(dst)

	if err != nil {
		panic(err)
		return
	}

	defer dstFile.Close()

	return io.Copy(dstFile, srcFile)
}
