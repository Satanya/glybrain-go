package event

type Event interface {
	GetContext() string
}

type commandEvent struct {
	context string
}

func (c *commandEvent) GetContext() string {
	return c.context
}
