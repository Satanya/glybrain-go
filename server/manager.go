package server

type ServerManager interface {
	Create()
	Register()
	Remove()
	//SetChannel
	GetCheckerMap()
	GetServersMap()
	GetUnregisterServersMap()
	Destroy()
}
